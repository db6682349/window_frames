SELECT 
    WEEK(date) AS week_number,
    YEAR(date) AS year_number,
    SUM(amount) AS weekly_sales,
    SUM(amount) OVER (ORDER BY WEEK(date)) AS CUM_SUM,
    CASE 
        WHEN DAYOFWEEK(date) = 2 THEN
            ROUND(
                (LAG(amount, 2) OVER (ORDER BY date) + LAG(amount, 1) OVER (ORDER BY date) + amount) / 3,
                2
            )
        WHEN DAYOFWEEK(date) = 6 THEN
            ROUND(
                (LAG(amount, 1) OVER (ORDER BY date) + amount + LEAD(amount, 1) OVER (ORDER BY date)) / 3,
                2
            )
        ELSE NULL
    END AS CENTERED_3_DAY_AVG
FROM 
    sales_data
WHERE 
    YEAR(date) = 1999 AND WEEK(date) IN (49, 50, 51)
GROUP BY 
    year_number, week_number
ORDER BY 
    year_number, week_number;